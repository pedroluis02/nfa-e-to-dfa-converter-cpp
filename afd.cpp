#include "afd.h"

AFD::AFD()
{
    //estados
    EstadosAfd = new string[100];
    //transiciones
    TransicionesAfd = new string[100];
}

//busqueda de si se encuentra un estado
char AFD::estadoAfdTabla(string _estadoAfd)
{
    if(_estadoAfd=="$")
        return '$';

    else
    {
        char encontrado1 = 'A';
        for(int i=0;i<num_estadosAfd;i++)
        {
            if(_estadoAfd==EstadosAfd[i])
                break;

            else
                encontrado1++;
        }
        return encontrado1;
    }
}

//busqueda de un estado final
string AFD::busquedaEstadoFinal(string _estado)
{
    string tem="";
    char s='A';
    for(int i=0;i<num_estadosAfd;i++)
    {
        string cad = EstadosAfd[i];
        string *des = new string[cad.size()/2];
        int t1 = separa(cad,des);
        for(int i=0;i<t1;i++)
        {
            if(_estado==des[i])
            {
                tem += s;
                break;
            }
        }
        s++;
    }
    if(tem=="")
        return "";
    else
        return tem;
}

int AFD::separa(string cadenaEstados,string vectorEstados[])
{
    cadenaEstados = cadenaEstados+"q";
    int indice=0;
    string tem = "";
    tem += cadenaEstados[0];
    for(int i=1;i<cadenaEstados.size();i++)
    {
        if(cadenaEstados[i]=='q')
        {
            vectorEstados[indice] = tem;
            indice++;
            tem = "q";
        }
        else
            tem += cadenaEstados[i];
    }
    return indice;
}

AFD::~AFD() {
    delete EstadosAfd;
    delete TransicionesAfd;
}
