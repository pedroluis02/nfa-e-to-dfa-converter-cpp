#include "afnd_e.h"

AFND_E::AFND_E()
{
    //estados
    EstadosAfnd_e = new string[100];
    //alfabeto
    AlfabetoAfnd_e = new char[100];
    //estados finales
    EstadosFinalesAfnd_e = new string[100];
    //transiciones
    inicio = final = NULL;
}

void AFND_E::guardarTransicion(string _estado1,char _simbol, string _estadon)
{
    if(inicio==NULL)
        inicio = final = new NodoTrans(_estado1,_simbol,_estadon,final);
    else
        final = final->sgte = new NodoTrans(_estado1,_simbol,_estadon,final->sgte);
}

/**********busqueda en Afnd************/
//busqueda de una transicion
string AFND_E::estadoLlegada(string _estado1,char _simbol)
{
    NodoTrans *aux = inicio;
    string e_transitado="";
    for(int i=0;i<num_transicionesAfnd_e;i++)
    {
        if(_estado1==aux->estadoPartida&&_simbol==aux->simboloLectura)
        {
            e_transitado += aux->estadoLlegada;
            aux = aux->sgte;
        }

        else
            aux = aux->sgte;
    }
    if(e_transitado=="")
        return "$";
    else
        return e_transitado;
}

//buscamos las tansiciones de la propiedad clausura
string AFND_E::clausuraEstado(string _estado1,char _simbol1)
{
    NodoTrans *aux = inicio;
    string _estadoClausura="";
    while(aux!=NULL)
    {
        if(_estado1==aux->estadoPartida&&_simbol1==aux->simboloLectura)
            _estadoClausura += aux->estadoLlegada;

        aux = aux->sgte;
    }

    if(_estadoClausura=="")
        return "";

    else
        return _estadoClausura;
}

/**********Para converion**************/
//separa una cadena de estados en un vector de estados
int AFND_E::separa(string cadenaEstados,string vectorEstados[])
{
    cadenaEstados = cadenaEstados+"q";
    int indice=0;
    string tem = "";
    tem += cadenaEstados[0];
    for(int i=1;i<cadenaEstados.size();i++)
    {
        if(cadenaEstados[i]=='q')
        {
            vectorEstados[indice] = tem;
            indice++;
            tem = "q";
        }
        else
            tem += cadenaEstados[i];
    }
    return indice;
}

//ordena vector de estados
void AFND_E::ordena(string vectorEstados[],int n)
{
    int j=0;
    for(int i=1;i<n;i++)
    {
        string tem = vectorEstados[i];
        j = i-1;
        while(tem<vectorEstados[j])
        {
            vectorEstados[j+1] = vectorEstados[j];
            j--;
            if(j<0)break;//condicion de parada para while
        }
        vectorEstados[j+1] = tem;
    }
}
//elimna los estados repetidos del vector de estados
int AFND_E::eliminarRepetidos(string vectorEstados[], int n)
{
    for(int i=0;i<n;i++)
    {
        for(int j=n-1;j>=i+1;j--)
        {
            if(vectorEstados[i]==vectorEstados[j])
            {
                for(int k=j;k<n-1;k++)
                {
                    vectorEstados[k] = "";
                    vectorEstados[k] = vectorEstados[k+1];
                }
                n--;
            }
        }
    }
    return n;
}
//funcion DELTA
string AFND_E::delta(string _estado,char _simbol)
{
    string __estado = estadoLlegada(_estado,_simbol);

    if(__estado=="$")
        return "";
    else
        return __estado;
}

//por cada estadode partida y simbolo buscamos su estado de llegada
string AFND_E::producto(string vectorEstados[], int n,char _simbol)
{
    string tem="";
    for(int i=0;i<n;i++)
    {
        tem += delta(vectorEstados[i],_simbol);
    }
    return tem;
}

//funcion clausura
//encontrar estado con clausura
string AFND_E::clausura(string _estado,char _simbol)
{
    string tem = clausuraEstado(_estado,_simbol);
    if(tem=="")
        return _estado;
    else
        return _estado+clausura(tem,_simbol);
}

//aplicamos clausura a cada uno de lo estado de la funcion producto
string AFND_E::extraerClausura(string __estados,char _simbol)
{
    string tem1;
    string tem2;
    string aux="";

    tem1 = tem2 = clausura(__estados,_simbol);

    string *desc = new string[tem2.size()/2];
    int t1 = separa(tem2,desc);
    for(int i=0;i<t1;i++)
    {
        tem1 += clausura(desc[i],_simbol);
    }

    string *descomp = new string[tem1.size()/2];
    int d1 = separa(tem1,descomp);
    ordena(descomp,d1);
    int d2 = eliminarRepetidos(descomp,d1);
    for(int i=0;i<d2;i++)
    {aux += descomp[i];}
    return aux;
}

string AFND_E::productoClausura(string vector[],int n,char _simbol)
{
    string tem="";
    for(int i=0;i<n;i++)
    {
        tem += extraerClausura(vector[i],_simbol);
    }
    return tem;
}

//buscar en el vector de estados si se encuentra un estado
bool AFND_E::encontradoAfd(string vector[],int n,string buscado)
{
    bool encontrado=false;
    for(int i=0;i<n;i++)
    {
        if(buscado==vector[i])
        {
            encontrado = true;
            break;
        }
    }
    return encontrado;
}


//converiosn de Afnd-e a Afd
AFD* AFND_E::convertir()
{
    AFD *at = new AFD();

    int len1 = 0;
    int len2 = 1;
    //aplicamos clausura al 1er estado
    string *V = new string[1];
    V[0] = EstadosAfnd_e[0];
    string cad = productoClausura(V,1,'#');
    string cad1="";
    string *D = new string[cad.size()/2];
    int H = separa(cad,D);
    ordena(D,H);
    int T = eliminarRepetidos(D,H);
    for(int i=0;i<T;i++)
    {cad1 += D[i];}
    at->EstadosAfd[len1] = cad1;

    while(len1<=len2-1)
    {
        for(int i=0;i<num_simbolosAfnd_e;i++)
        {
            string tem1  = at->EstadosAfd[len1];
            string *desc = new string[tem1.size()/2];
            int s2 = separa(tem1,desc);
            int lon = eliminarRepetidos(desc,s2);
            string tem2 = producto(desc,lon,AlfabetoAfnd_e[i]);
            if(tem2=="")
            {
                at->TransicionesAfd[at->num_transAfd] = "$";
                at->num_transAfd++;
            }
            else
            {
                string *des = new string[tem2.length()/2];
                int z1 = separa(tem2,des);
                ordena(des,z1);
                int t1 = eliminarRepetidos(des,z1);

                string cadena1 = productoClausura(des,t1,'#');

                string cadena2="";
                string *DE = new string[cadena1.size()/2];
                int x1 = separa(cadena1,DE);
                ordena(DE,x1);
                int t2 = eliminarRepetidos(DE,x1);
                for(int j=0;j<t2;j++)
                    cadena2 += DE[j];

                at->TransicionesAfd[at->num_transAfd] = cadena2;
                at->num_transAfd++;

                if(encontradoAfd(at->EstadosAfd,len2,cadena2)==false)
                {
                    at->EstadosAfd[len2] = cadena2;
                    len2++;
                }
            }
        }
        len1++;
    }
    at->num_estadosAfd = len2;

    return at;
}

// desctructor de la clse liberamos memoria
AFND_E::~AFND_E()
{
    delete EstadosAfnd_e;
    delete AlfabetoAfnd_e;
    delete EstadosFinalesAfnd_e;
    delete inicio;
    delete final;
}
