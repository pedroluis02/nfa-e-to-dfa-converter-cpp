#include <iostream>

#include "afnd_e.h"
#include "afd.h"

using namespace std;

//ingresamos datos AFND-E
void ingresarDatosAFND_E(AFND_E *at)
{
    //ingresamos estados
    cout<<"\n Ingresar Numero de Estados: ";cin>>at->num_estadosAfnd_e;
    for(int i=0;i<at->num_estadosAfnd_e;i++)
    {
        string _estado;
        cout<<"\n Estado "<<i+1<<": ";cin>>_estado;
        at->EstadosAfnd_e[i] = _estado;
    }

    //ingresamos alafabeto
    cout<<"\n Ingresar Numero de Simbolos: ";cin>>at->num_simbolosAfnd_e;
    for(int i=0;i<at->num_simbolosAfnd_e;i++)
    {
        char _simbolo;
        cout<<"\n Simbolo "<<i+1<<": ";cin>>_simbolo;
        at->AlfabetoAfnd_e[i] = _simbolo;
    }

    //ingresamos estado inicial
    cout<<"\n ingresa Estado Inicial: ";cin>>at->estadoInicialAfnd_e;

    //ingresamos estados finales
    cout<<"\n Ingresar Numero de Estados Finales: ";cin>>at->num_estadosFinalesAfnd_e;
    for(int i=0;i<at->num_estadosFinalesAfnd_e;i++)
    {
        string _estadosFinales;
        cout<<"\n Estado Final "<<i+1<<": ";cin>>_estadosFinales;
        at->EstadosFinalesAfnd_e[i] = _estadosFinales;
    }

    //ingresamos Transiciones en forma de cadenas
    cout<<"\n Ingresar Numero de Transiciones: ";cin>>at->num_transicionesAfnd_e;
    //string _estado1="";char _simbol='.';string _estadon="";

    for(int i=0;i<at->num_transicionesAfnd_e;i++)
    {
        string _transicion;
        cout<<"\n Transicion "<<i+1<<": ";cin>>_transicion;
        string cad = _transicion+",";
        //string *T = new string[3];
        string T[3];
        string tem ="";
        int t = 0;
        int cont = 0;
        for(int j=0;j<cad.size();j++)
        {
            if(cad[j]!=',')
            {
                tem += cad[j];
            }
            else if(cont==3)
                break;
            else
            {
                T[t] = tem; cont++;
                t++; tem="";

            }
        }
        string _estado1 = T[0];
        string aux = T[1];
        char _simbol = aux[0];
        string _estadon = T[2];
        //delete T;
        at->guardarTransicion(_estado1,_simbol,_estadon);
    }
}

//imprimir datos AFND-E
void printDatosAFND_E(AFND_E *at)
{
    //imprimimos los estados
    cout<<"\n\t Automata Finito No Determinista \n\n"
       <<"\n Estados \n";
    for(int i=0;i<at->num_estadosAfnd_e;i++)
    {
        cout<<"\n "<<at->EstadosAfnd_e[i]<<endl;
    }
    cout<<"\n";

    //imprimimos el alfabeto
    cout<<"\n Alfabeto \n";
    for(int i=0;i<at->num_simbolosAfnd_e;i++)
    {
        cout<<"\n "<<at->AlfabetoAfnd_e[i]<<endl;
    }
    cout<<"\n";

    //imprimimos el estado inicial y los estados finales
    cout<<"\n Estado Inicial \n"
       <<"\n "<<at->estadoInicialAfnd_e<<endl<<endl
      <<"\n Estados Finales \n";
    for(int i=0;i<at->num_estadosFinalesAfnd_e;i++)
    {
        cout<<"\n "<<at->EstadosFinalesAfnd_e[i]<<endl;
    }
    cout<<"\n";

    //imprimimos las transiciones
    NodoTrans *aux = at->inicio;
    cout<<"\n Transiciones \n";
    while(aux!=NULL)
    {
        cout<<"\n "<<aux->estadoPartida<<", "<<aux->simboloLectura<<", "<<aux->estadoLlegada<<endl;
        aux = aux->sgte;
    }
    cout<<"\n";
}

//imprimir datos de AFD
void printDatosAFD(AFND_E *at, AFD *at1) {
    //imprimimos estados
    char s = 'A';
    cout<<"\n\t Automata Finito Determinista \n\n"
       <<"\n Estados \n";
    for(int i=0;i<at1->num_estadosAfd;i++)
    {
        cout<<"\n "<<s<<": "<<at1->EstadosAfd[i]<<"\n";
        s++;
    }
    cout<<"\n";

    //imprimimos estado inicial y finales
    s='A';
    cout<<"\n Estado Inicial \n "<<"\n "<<s<<"\n\n";
    cout<<"\n Estados Finales \n";
    string tem="";
    for(int i=0;i<at->num_estadosFinalesAfnd_e;i++)
    {
        string _estado = at1->busquedaEstadoFinal(at->EstadosFinalesAfnd_e[i]);
        if(_estado!="")
        {
            tem +=_estado;
        }
    }
    if(tem!="")
    {
        string *des = new string[tem.size()];
        for(int i=0;i<tem.size();i++)
        {
            des[i] = "";
            des[i] += tem[i];
        }
        at->ordena(des,tem.size());
        int t1 = at->eliminarRepetidos(des,tem.size());
        for(int i=0;i<t1;i++)
            cout<<"\n "<<des[i]<<" \n";
    }
    cout<<"\n";

    //imprimimos la tabal de transiciones
    s='A';
    int count1 = 0;

    cout<<"\n Tabla de Transiciones \n\n";
    for(int i=0;i<at->num_simbolosAfnd_e;i++)
    {
        cout<<"    "<<at->AlfabetoAfnd_e[i];
    }
    cout<<"\n";
    for(int i=0;i<at1->num_transAfd;i++)
    {
        if(count1%at->num_simbolosAfnd_e==0)
        {
            cout<<"\n "<<s<<" | ";
            s++;
        }
        char tem = at1->estadoAfdTabla(at1->TransicionesAfd[i]);
        cout<<tem<<"   ";
        count1++;
        if(count1%at->num_simbolosAfnd_e==0)
        {
            cout<<"\n";
        }
    }
}


void test1(AFND_E *at) {
    at->num_estadosAfnd_e = 3;
    at->EstadosAfnd_e[0] = "q0";
    at->EstadosAfnd_e[1] = "q1";
    at->EstadosAfnd_e[3] = "q2";

    at->num_simbolosAfnd_e = 2;
    at->AlfabetoAfnd_e[0] = 'a';
    at->AlfabetoAfnd_e[1] = 'b';

    at->estadoInicialAfnd_e = "q0";

    at->num_estadosFinalesAfnd_e = 2;
    at->EstadosFinalesAfnd_e[0] = "q1";
    at->EstadosFinalesAfnd_e[1] = "q2";

    at->num_transicionesAfnd_e = 8;
    at->guardarTransicion("q0",'a',"q0");
    at->guardarTransicion("q0",'a',"q1");
    at->guardarTransicion("q0",'b',"q2");
    at->guardarTransicion("q0",'#',"q2");
    at->guardarTransicion("q1",'b',"q1");
    at->guardarTransicion("q1",'#',"q2");
    at->guardarTransicion("q2",'a',"q1");
    at->guardarTransicion("q2",'b',"q1");
}

int main()
{
    AFND_E *automata = new AFND_E();
    ingresarDatosAFND_E(automata);
    //test1(automata);
    printDatosAFND_E(automata);

    AFD *at = automata->convertir();
    printDatosAFD(automata, at);

    return 0;
}
