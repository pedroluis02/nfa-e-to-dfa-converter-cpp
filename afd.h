#ifndef AFD_H
#define AFD_H

#include <iostream>
#include <cstring>

using namespace std;

class AFD
{
public:
    //estados
    string *EstadosAfd;
    int num_estadosAfd;

    //tabla de Transiciones
    string *TransicionesAfd;
    int num_transAfd;

    AFD();

    bool encontradoAfd(string vector[],int n,string buscado);
    char estadoAfdTabla(string _estadoAfd);
    string busquedaEstadoFinal(string _estado);
    int separa(string cadenaEstados,string vectorEstados[]);

    ~AFD();
};

#endif // AFD_H
