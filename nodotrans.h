#ifndef NODOTRANS_H
#define NODOTRANS_H

#include <iostream>
#include <cstring>

using namespace std;

class NodoTrans
{
public:
    string estadoPartida;
    char simboloLectura;
    string estadoLlegada;

    NodoTrans *sgte;//acceder a la siguiente transicion

    NodoTrans(string estado1,char simbol, string estadon, NodoTrans *sgte1);
    NodoTrans(string estado1, char simbol, string estadon);

    ~NodoTrans();
};

#endif // NODOTRANS_H
