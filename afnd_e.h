#ifndef AFND_E_H
#define AFND_E_H

#include "nodotrans.h"
#include "afd.h"

class AFND_E
{
public:
    /*----Para el Afnd-e---*/
    //estados
    string *EstadosAfnd_e;
    string estadoAfnd_e;
    int num_estadosAfnd_e;

    //alfabeto
    char *AlfabetoAfnd_e;
    char simbolo;
    int num_simbolosAfnd_e;

    //estado inicial
    string estadoInicialAfnd_e;

    //estados finales
    string *EstadosFinalesAfnd_e;
    string estadoFinalAfnd_e;
    int num_estadosFinalesAfnd_e;

    //transiciones
    NodoTrans *inicio;
    NodoTrans *final;
    string transicionAfnd_e;
    int num_transicionesAfnd_e;

    AFND_E();

    void guardarTransicion(string _estado1,char _simbol, string _estadon);
    bool encontradoAfd(string vector[],int n,string buscado);

    string estadoLlegada(string _estado1,char _simbol);
    string clausuraEstado(string _estado1,char _simbol1);

    int separa(string cadenaEstados,string vectorEstados[]);
    void ordena(string vectorEstados[],int n);
    int eliminarRepetidos(string vectorEstados[], int n);

    string delta(string _estado,char _simbol);
    string producto(string vectorEstados[], int n,char _simbol);
    string clausura(string _estado,char _simbol);
    string extraerClausura(string __estados,char _simbol);
    string productoClausura(string vector[],int n,char _simbol);

    AFD* convertir();

    ~AFND_E();
};

#endif // AFND_E_H
